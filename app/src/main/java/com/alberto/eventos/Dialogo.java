package com.alberto.eventos;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

/**
 * Created by Alberto on 28/02/2017.
 */

public class Dialogo extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle extras = getIntent().getExtras();
        if (getIntent().hasExtra("mensaje")) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Mensaje:");
            String mensaje  = extras.getString("mensaje");
            final String evento  = extras.getString("evento");
            alertDialog.setMessage(mensaje);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "CERRAR",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            if(evento != null){
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ABRIR EVENTO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Context context = EventosAplicacion.getAppContext();
                                Intent intent = new Intent(context, EventoDetalles.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("evento", evento);
                                context.startActivity(intent);

                            }
                        });
            }
            alertDialog.show();
            extras.remove("mensaje");
        }
    }
}